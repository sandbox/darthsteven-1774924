// Inject the actual Dreditor script as per:
// http://stackoverflow.com/questions/9515704/building-a-chrome-extension-inject-code-in-a-page-using-a-content-script/9517879#9517879

// As we're injecting the code for dreditor, we need to trick the Content Scope
// Runner into thinking it's already run and we also remove the last update
// check time so that Dreditor doesn't do update checks, as Chrome will do that
// for us.
var actualCode = [
  'var __PAGE_SCOPE_RUN__ = true;',
  "if ('localStorage' in window && window['localStorage'] !== null) {",
  "  window['localStorage'].removeItem('Dreditor.lastUpdateCheck');",
  "}"
].join('\n');

var script = document.createElement('script');
script.textContent = actualCode;
(document.head||document.documentElement).appendChild(script);
script.parentNode.removeChild(script);

var s = document.createElement('script');
s.src = chrome.extension.getURL("dreditor.user.js");
s.onload = function() {
    this.parentNode.removeChild(this);
};
(document.head||document.documentElement).appendChild(s);
